{# Oracle Java.  Now running 7u40 #}

java-oracle:
  server-jre:
    md5: ba3a8e930d8dac68e965eb775ef7ef97
    version: 7u45
    dir: jdk1.7.0_45
    path: /usr/java/latest/bin
