# This is my Salt State top file
# Comments are legit, so I'll be frequently using them.

base:
  '*':
    - vim
    - tmux

  'logstash':
    - java-oracle
    - elasticsearch
    - redis-server
    - logstash
    - logstash.agent
    - nginx
    - kibana
    
