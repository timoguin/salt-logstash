{#
    Vim Salt State
    prepared by: bobby

    The requirement on vim-doc is to try timing during install.  
    This could be removed for production purposes.  Added support for 
    update-alternatives for ubuntu users
#}

vim:
  pkg:
    - installed
{% if grains['os'] == 'BROKENUNTIL-0.17' %}

vim-default:
  alternatives:
    - set:
      - name: editor
      - path: /usr/bin/vim.basic
    - require:
      - pkg: vim

{% endif %}

vim-doc:
  pkg:
    - installed
    - require:
      - pkg: vim
