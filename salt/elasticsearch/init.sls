# Oracle version of Java runtime. 
include:
  - java-oracle

elasticsearch-package:
  pkg.installed:
    # The actual package name is 'elasticsearch:all', I needed to remove :all
    - sources:
      - elasticsearch: salt://elasticsearch/elasticsearch-0.90.7.deb

elasticsearch-config:
  file.managed:
    - name: /etc/elasticsearch/elasticsearch.yml
    - source: salt://elasticsearch/config/elasticsearch.yml
    - template: jinja 
    - require:
      - pkg: elasticsearch-package

elasticsearch-service:
  service.running:
    - name: elasticsearch
    - watch:
      - pkg: elasticsearch-package

