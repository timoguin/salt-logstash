{#  UFW For Ubuntu Linux #}

ufw:
  pkg:
    - installed
  service:
    - enable: True
    - running
    - require:
      - pkg: ufw

ufw-enable-salt:
  cmd.run:
    - name: ufw allow salt
    - require:
      - pkg: ufw

ufw-enable-ssh:
  cmd.run:
    - name: ufw allow ssh
    - require:
      - pkg: ufw

